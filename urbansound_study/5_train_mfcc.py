import os

import torch
from torch.utils.data import Dataset

from urbansound_4_my_read_dataset_mfc import UrbanSoundDatasetMFC

if __name__ == '__main__':

    DATASET = '/home/mint/Datasets/UrbanSound8K/'
    csv_path = os.path.join(DATASET, 'metadata/UrbanSound8K.csv')
    file_path = os.path.join(DATASET, 'audio/')
    batch_size = 5
    input_size = 40
    output_size = 10
    lr = 0.1

    trainset = UrbanSoundDatasetMFC(csv_path, file_path, [1, 2])
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=False, num_workers=2)

    model = torch.nn.Sequential(
        torch.nn.Linear(input_size, 128),
        torch.nn.ReLU(),
        torch.nn.Linear(128, 64),
        torch.nn.ReLU(),
        torch.nn.Linear(64, output_size),
        torch.nn.Sigmoid()
    )
    criterion = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=lr)

    for batch_idx, (data, labels) in enumerate(trainloader):
        pred = model(data)
        loss = criterion(pred, labels)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        print(f'Batch: {batch_idx}, loss: {loss.item()}')
        exit()
