import librosa
import numpy as np
import pandas as pd
import torch
from torch.utils.data import Dataset


class UrbanSoundDatasetMFC(Dataset):
    def __init__(self, csv_path, file_path, folderList):
        csvData = pd.read_csv(csv_path)
        self.file_names = []
        self.labels = []
        self.folders = []
        for i in range(0, len(csvData)):
            if csvData.iloc[i, 5] in folderList:
                self.file_names.append(csvData.iloc[i, 0])
                self.labels.append(csvData.iloc[i, 6])
                self.folders.append(csvData.iloc[i, 5])

        self.file_path = file_path
        self.folderList = folderList

    def _extract_mfcc(self, path):
        audio, sr = librosa.load(path)
        mfccs = librosa.feature.mfcc(audio, sr, n_mfcc=40)
        return np.mean(mfccs.T, axis=0)

    def __getitem__(self, index):
        path = self.file_path + "fold" + str(self.folders[index]) + "/" + self.file_names[index]
        sound = self._extract_mfcc(path)
        sound = torch.tensor(sound)

        return sound, self.labels[index]

    def __len__(self):
        return len(self.file_names)


if __name__ == '__main__':
    import os

    DATASET = '/home/mint/Datasets/UrbanSound8K/'
    csv_path = os.path.join(DATASET, 'metadata/UrbanSound8K.csv')
    file_path = os.path.join(DATASET, 'audio/')
    batch_size = 5

    trainset = UrbanSoundDatasetMFC(csv_path, file_path, [1, 2])
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=False, num_workers=2)

    for batch_idx, (data, labels) in enumerate(trainloader):
        print(data.shape)
        exit()
