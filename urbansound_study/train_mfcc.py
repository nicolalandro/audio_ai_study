# try:
#     import src
# except:
#     import sys
#
#     sys.path.insert(0, './')
import argparse
import datetime
import os
import time

import librosa
import numpy as np
import torch
from ignite.metrics import Average, Accuracy

from urbansound_study.urbansound_dataloader import UrbanSoundDataset

parser = argparse.ArgumentParser("BERT")
parser.add_argument('--path', type=str, default='/media/mint/Barracuda/Datasets/UrbanSound8K')
parser.add_argument('--dataset', type=str, default='urbansound', choices=['urbansound'])

parser.add_argument('--gpu', type=str, default='0', help='select the gpu or gpus to use')

parser.add_argument('--batch-size', type=int, default=256)
parser.add_argument('--lr', type=float, default=0.01, help="learning rate for optimizer")
parser.add_argument('--max-epoch', type=int, default=50)
parser.add_argument('--save-freq', type=int, default=50000)

args = parser.parse_args()


def main():
    print_all_args()

    device = test_gpu_settings()

    def transform_audio(path):
        audio, sr = librosa.load(path)
        mfccs = librosa.feature.mfcc(audio, sr, n_mfcc=40)
        return np.mean(mfccs.T, axis=0)

    if args.dataset == 'urbansound':
        num_labels = 10
        csv_path = os.path.join(args.path, 'metadata/UrbanSound8K.csv')
        file_path = os.path.join(args.path, 'audio/')

        trainset = UrbanSoundDataset(csv_path, file_path, range(1, 10), transform_audio)
        testset = UrbanSoundDataset(csv_path, file_path, [10], transform_audio)

    trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size, shuffle=False, num_workers=2)
    testloader = torch.utils.data.DataLoader(testset, batch_size=args.batch_size, shuffle=False, num_workers=2)

    model = torch.nn.Sequential(
        torch.nn.Linear(40, 128),
        torch.nn.ReLU(),
        torch.nn.Linear(128, 64),
        torch.nn.ReLU(),
        torch.nn.Linear(64, num_labels),
        torch.nn.Sigmoid()
    )
    if device == 'cuda':
        model = torch.nn.DataParallel(model)
    model = model.to(device)
    criterion = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=args.lr)

    start_time = time.time()
    for epoch in range(args.max_epoch):
        print("==> Start Epoch {}/{}".format(epoch + 1, args.max_epoch))
        train(model, trainloader, criterion, optimizer)

        elapsed = round(time.time() - start_time)
        elapsed = str(datetime.timedelta(seconds=elapsed))
        print("Finished(Train). Total elapsed time (h:m:s): {}".format(elapsed))

        print("- Test Epoch {}/{}".format(epoch + 1, args.max_epoch))
        test(model, testloader, criterion)
        print("Finished(Test). Total elapsed time (h:m:s): {}".format(elapsed))

        if (epoch + 1) % args.save_freq == 0:  # Save trained model
            print("Model trained saveing...")
            torch.save(model,
                       f'{args.save_dir}/{args.model}_{epoch + 1}.pt')
            print("Finished(Save). Total elapsed time (h:m:s): {}".format(elapsed))
        print("==> End Epoch {}/{}".format(epoch + 1, args.max_epoch))


def train(model, trainloader, criterion, optimizer):
    model.train()
    xent_losses = Average()

    for batch_idx, (data, labels) in enumerate(trainloader):
        labels = labels.cuda(non_blocking=True)
        data = data.cuda(non_blocking=True)

        predictions = model(data)
        loss = criterion(predictions, labels)

        optimizer.zero_grad()
        loss.backward()

        optimizer.step()

        xent_losses.update(loss.item())

        if (batch_idx + 1) % 1 == 0:
            print(
                f"Batch {batch_idx + 1}\t CrossEntropy(Batch) {loss.item()} ({xent_losses.compute()})")


def test(model, testloader, criterion):
    model.eval()

    accuracy_metric = Accuracy()
    xent_losses = Average()

    with torch.no_grad():
        for data, labels in testloader:
            labels = labels.cuda(non_blocking=True)
            data = data.cuda(non_blocking=True)

            predictions = model(data)

            loss = criterion(predictions, labels)
            xent_losses.update(loss.item())

            accuracy_metric.update((predictions, labels))

    test_crossentropy_loss = xent_losses.compute()
    accuracy = accuracy_metric.compute()
    print(
        f'    Test Crossentropy(Batch): {test_crossentropy_loss}, Accuracy({accuracy_metric._type}): {accuracy}')
    return accuracy


def print_all_args():
    print('--- START Arg parse ---')
    options = vars(args)
    for k, v in options.items():
        print(k, ':', v)
    print('--- END Arg parse ---')


def test_gpu_settings():
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    use_gpu = torch.cuda.is_available()

    if use_gpu:
        print(f"Currently using GPU: {args.gpu}")
        return 'cuda'
    else:
        print("Currently using CPU")
        return 'cpu'


if __name__ == '__main__':
    main()
