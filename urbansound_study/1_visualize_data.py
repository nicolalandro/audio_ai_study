# https://medium.com/@aakash__/classifying-audio-using-pytorch-84861f3505ea

import os

import librosa.display
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

DATASET = '/home/mint/Datasets/UrbanSound8K/'

csv_path = os.path.join(DATASET, 'metadata/UrbanSound8K.csv')
df = pd.read_csv(csv_path)

classes = list(df["class"].unique())
print(f'num classes: {len(classes)}')
paths = dict()
for i in range(len(classes)):
    temp_df = df[df["class"] == classes[i]].reset_index()
    fold = temp_df["fold"].iloc[0]  # The fold of the first audio sample for the specific class
    sample_name = temp_df["slice_file_name"].iloc[0]
    path = os.path.join(DATASET, 'audio', f'fold{fold}', sample_name)
    paths[classes[i]] = path

for i, label in enumerate(classes):
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3)

    sample = paths[label]
    data, sample_rate = librosa.load(sample)
    librosa.display.waveplot(data, sr=sample_rate, ax=ax1)

    mfccs = librosa.feature.mfcc(data, sample_rate, n_mfcc=40)
    ax2.imshow(mfccs, interpolation='nearest', origin='lower')

    mfcc_mean = np.mean(mfccs.T, axis=0)
    ax3.bar(mfcc_mean, range(0, len(mfcc_mean)))
    plt.show()
    break
