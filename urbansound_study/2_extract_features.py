# https://medium.com/@aakash__/classifying-audio-using-pytorch-84861f3505ea

import os

import librosa
import librosa.display
import numpy as np
import pandas as pd
import torch

DATASET = '/home/mint/Datasets/UrbanSound8K/'

csv_path = os.path.join(DATASET, 'metadata/UrbanSound8K.csv')
df = pd.read_csv(csv_path)

classes = list(df["class"].unique())


# Helper function to generate mfccs
def extract_mfcc(path):
    audio, sr = librosa.load(path)
    mfccs = librosa.feature.mfcc(audio, sr, n_mfcc=40)
    return np.mean(mfccs.T, axis=0)


features = []
labels = []
folds = []

for i in range(len(df)):
    print(f'\r{i + 1}/{len(df)}', end='')
    fold = df["fold"].iloc[i]
    filename = df["slice_file_name"].iloc[i]
    path = os.path.join(DATASET, 'audio', f'fold{fold}', filename)
    mfccs = extract_mfcc(path)
    features.append(mfccs)
    folds.append(fold)
    labels.append(df["classID"].iloc[i])

print('\ntorch features')
features = torch.tensor(features)
labels = torch.tensor(labels)
folds = torch.tensor(folds)

print('save torch features')
torch.save(features, "data/features_mfccs.pt")
torch.save(labels, "data/labels.pt")
torch.save(folds, "data/folds.pt")
